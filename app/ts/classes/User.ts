import { sha256, sha224 } from 'js-sha256';
import {Reminder} from "./Reminder";
export class User {

    public user: string[];
    private username: string;
    private password: string;
    id: string;

    constructor(username: string, password: string ) {
        this.username = username;

        if ( password.length > 63 )
            this.password = password;

        this.password = sha256(password);
    }

    login() {
        const url = 'http://127.0.0.1:8000/api/users/login';
        const form = new FormData;

        form.append('username', this.username);
        form.append('password', this.password);

        fetch( url, {
            method: 'POST',
            body: form
        })
            .then( data => data.json() )
            .then( response => {
                this.id = response.id;
                console.log(response);
            }).catch( ( error ) => {
                console.log(error);
            });
    }

    apiKey() {
        var d = new Date().getTime();

        if( window.performance && typeof window.performance.now === "function" )
        {
            d += performance.now();
        }

        var key = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c)
        {
            var r = (d + Math.random()*16)%16 | 0;
            d = Math.floor(d/16);
            return (c=='x' ? r : (r&0x3|0x8)).toString(16);
        });

        return key;
    }

    saveKey(id: string, key: string) {

        const str_user = JSON.stringify( [this.id, key] );
        localStorage.setItem('user', str_user );
    }

    register(key: string) {
        const url = 'http://127.0.0.1:8000/api/users/create';
        const form = new FormData;

        form.append('username', this.username);
        form.append('password', this.password);
        form.append('api_key', key);

        fetch( url, {
            method: 'POST',
            body: form
        })
            .then( data => data.json() )
            .then( response => {
                this.id = response.id;
                console.log(this.id);
            });


    }

    passwordIsGood(repassword:  string) {

        repassword = sha256(repassword);
        return repassword == this.password;
    }
}