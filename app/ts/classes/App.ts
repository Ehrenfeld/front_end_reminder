import {Reminder} from "./Reminder";
import {User} from "./User";

interface ReminderLiteral {
    id: number;
    user_id: number;

    title: string;
    content: string;
    date_debut: string;
    date_fin: string;
}

class App {

// <editor-fold desc="My Property of App">
    public reminders: Array<Reminder>;
    reminder_deletable: Array<Reminder>;

    public $container: HTMLDivElement;
    public $form: HTMLFormElement;
    public $title: HTMLInputElement;
    public $description: HTMLTextAreaElement;
    public $date_debut: HTMLInputElement;
    public $date_fin: HTMLInputElement;
    public $alert_container: HTMLDivElement;
    public $list_3_day: HTMLUListElement;
    public $list_to_day: HTMLUListElement;
    public $icon_close: HTMLElement;
    public $reload_btn: HTMLButtonElement;
    public $delete_btn: HTMLButtonElement;
    $charging: HTMLDivElement;
    $form_register: HTMLFormElement;
    $username: HTMLInputElement;
    $repassword: HTMLInputElement;
    $password: HTMLInputElement;
    public $form_login: HTMLFormElement;
    public $username_login: HTMLInputElement;
    public $password_login: HTMLInputElement;
    $div_log: HTMLDivElement;
    public $div_principale: HTMLDivElement;
// </editor-fold>

    constructor() {

        this.reminders = [];
        this.reminder_deletable = [];

        this.$container = <HTMLDivElement>document.getElementById('container');

        this.$charging = <HTMLDivElement>document.getElementById('charging');

        this.$alert_container = <HTMLDivElement>document.getElementById('alert-container');
        this.$icon_close = <HTMLElement>document.getElementById('close');
        this.$list_to_day = <HTMLUListElement>document.getElementById('list-to-day');
        this.$list_3_day = <HTMLUListElement>document.getElementById('list-3-day');
        
        this.$form = <HTMLFormElement>document.getElementById('add-reminder');
        
        this.$reload_btn = <HTMLButtonElement>document.getElementById('reload-btn');
        this.$delete_btn = <HTMLButtonElement>document.getElementById('delete-btn');

        this.$title = <HTMLInputElement>document.getElementById('title');
        this.$description = <HTMLTextAreaElement>document.getElementById('description');
        this.$date_debut = <HTMLInputElement>document.getElementById('date-debut');
        this.$date_fin = <HTMLInputElement>document.getElementById('date-fin');

        this.$form_register = <HTMLFormElement>document.getElementById('form-register');
        this.$username = <HTMLInputElement>document.getElementById('username');
        this.$password = <HTMLInputElement>document.getElementById('password');
        this.$repassword = <HTMLInputElement>document.getElementById('repassword');

        this.$form_login = <HTMLFormElement>document.getElementById('form-login');
        this.$username_login = <HTMLInputElement>document.getElementById('login-username');
        this.$password_login = <HTMLInputElement>document.getElementById('login-password');

        this.$div_log = <HTMLDivElement>document.getElementById('div-log');
        this.$div_principale = <HTMLDivElement>document.getElementById('div-principale');
    }

    init( user_id: string  ): Promise<Reminder[]> {
        const form = new FormData;
        form.append('user_id', user_id);

        let url = `http://127.0.0.1:8000/api/reminders`;
            return fetch( url, {
                method: 'POST',
                body: form
            })
                .then( response => response.json() )
                .then( (datas: ReminderLiteral[]) => {


                    const reminders: Reminder[] = [];
                    for ( var key in datas ) {

                        const reminder = new Reminder(datas[key].title,
                                                    datas[key].content,
                                                    datas[key].date_debut,
                                                    datas[key].date_fin);

                        reminder.id = datas[key].id;
                        reminders.push( reminder );
                    }

                    for (let reminder of reminders ) {

                        let r = reminder.date_debut.split('-');
                        let v = reminder.date_fin.split('-');

                        let year = parseInt(r[0]);
                        let day = parseInt(r[2]);
                        let month = parseInt(r[1]) - 1;

                        let year_fin = parseInt(v[0]);
                        let day_fin = parseInt(v[2]);
                        let month_fin = parseInt(v[1]) - 1;

                        const date_debut = new Date(year, month, day).toLocaleString();
                        let date_fin = new Date(year_fin, month_fin, day_fin).toLocaleString();
                        let date_debut_3 = new Date(year, month, day - 6).toLocaleString();

                        reminder.render(
                                        this.$list_to_day,
                                        this.$list_3_day,
                                        this.$alert_container,
                                        this.$container,
                                        date_debut, date_fin, date_debut_3 );

                        this.add(reminder);
                    }
                    return reminders;
                });
    }

    add ( reminder: Reminder ) {

        this.reminders.push( reminder );
        if (reminder.deletable) {
            this.reminder_deletable.push( reminder );
        }
    }

    save() {

        const str_reminder = JSON.stringify( this.reminders );
        localStorage.setItem('reminders', str_reminder);
    }

    get() {
        const str_json: string = localStorage.getItem('user') as string;

        let t = str_json.split(',');
        let b = t[0].split('[');

        return b[1];
    }

    getJson() {

        const  json: string = localStorage.getItem('user') as string;
        let t = JSON.parse(json);

        let user_id = t[0];
        let api_key = t[1];

        const url = 'http://127.0.0.1:8000/api/';
        const form = new FormData;

        form.append('id', user_id);
        form.append('api_key', api_key);

        fetch( url, {
            method: 'POST',
            body: form
        })
            .then( data => data.json() )
            .then( response => {

                let user =  new User(response[0].username, response[0].password);
                    user.id = response[0].id;

                user.login();

                this.$div_log.setAttribute('hidden', 'hidden');
                this.$div_principale.removeAttribute('hidden');

                this.init(user.id);

            }).catch( ( error ) => {
            console.log(error);
        });

    }

    delete ( id: string ) {

        const url = "http://127.0.0.1:8000/api/reminders/" + id + "/multidelete";

        fetch( url, {
            method: 'DELETE'
        })
            .then( data => data.json() )
            .then( response => {

                if ( response.success ) {
                    this.$container.remove();
                }
            });
    }

}
export default new App();