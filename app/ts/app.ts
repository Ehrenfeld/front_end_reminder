import '../scss/styles';
import app from './classes/App';
import '../../dist/semantic.js';
import {Reminder} from "./classes/Reminder";
import {User} from "./classes/User";

if ( localStorage.getItem('user') != null) {
    app.getJson();
    console.log('hjhgjghj');
}

(<any>$('.ui.accordion'))
    .accordion()
;

app.$form_register.addEventListener('submit', function (e) {

    e.preventDefault();
    const username = app.$username.value;
    const password = app.$password.value;
    const repassword = app.$repassword.value;

    let user =  new User(username, password);

    if ( user.passwordIsGood(repassword) && password.length >= 6) {

        let key = user.apiKey();
        user.register( key );

        setTimeout( () => {
            user.saveKey( user.id, key );
        }, 1000);
    }
    else {
        app.$form_register.children[0].removeAttribute('hidden');
    }
});

app.$form_login.addEventListener('submit', function (e) {

    e.preventDefault();
    const username = app.$username_login.value;
    const password = app.$password_login.value;

    let user =  new User(username, password);

    user.login( );
    setTimeout( () => {


        app.$div_log.setAttribute('hidden', 'hidden');
        app.$div_principale.removeAttribute('hidden');

        app.init(user.id);
    }, 1000);
});

app.$form.addEventListener('submit', function ( event ) {

    event.preventDefault();

    const title = app.$title.value;
    const description = app.$description.value;

    const date_debut = app.$date_debut.value;
    const date_fin = app.$date_fin.value;

    if ( title.length > 0 && description.length > 0 ) {

        const reminder = new Reminder(title, description, date_debut, date_fin);

        reminder.save( app.get() );

        let r = date_debut.split('-');
        let v = date_fin.split('-');

        let year = parseInt(r[0]);
        let day = parseInt(r[2]);
        let month = parseInt(r[1]) - 1;

        let year_fin = parseInt(v[0]);
        let day_fin = parseInt(v[2]);
        let month_fin = parseInt(v[1]) - 1;

        let date = new Date(year, month, day).toLocaleString();
        let date_de_fin = new Date(year_fin, month_fin, day_fin).toLocaleString();
        let date_debut_3 = new Date(year, month, day - 6).toLocaleString();

        reminder.render(
                        app.$list_to_day,
                        app.$list_3_day,
                        app.$alert_container,
                        app.$container,
                        date, date_de_fin, date_debut_3 );
        app.add(reminder);
    }
});

app.$icon_close.addEventListener('click', function ( event ) {
    app.$alert_container.className = 'ui info message hidden';
});

app.$reload_btn.addEventListener('click', function (event) {
    location.reload();
});

app.$delete_btn.addEventListener('click', function () {

    console.log(app.reminder_deletable.length);
    if ( app.reminder_deletable.length > 0) {

        app.$charging.removeAttribute('hidden');

        let id_string = '';
        for ( let reminder of app.reminder_deletable) {

            if (reminder.deletable) {
                id_string += reminder.id + ',';
            }
        }
        id_string = id_string.slice(0, id_string.length-1);
        app.delete(id_string);


        setInterval(() => {
            location.reload();
        }, 2000);
    }
    else {
        console.log('notototot');
    }
});



